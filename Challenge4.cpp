#include <iostream>
#include <iomanip>
#include <cmath>
#include <vector>
#include <cstdlib>
#include <ctime>

using namespace std;

void sort(vector<int> &v);
float mean(vector<int> &v);
int med(vector<int> &v);
int mode(vector<int> &v);

int main()
{
	vector<int>v;
	int even=0,odd=0,count;
	float me,median,size;
	
	for(int i=0; i<=rand()%101+50; i++)		//random number between 50-150
	{
		int r;
		r=rand()%100;
		v.push_back(r);
	}
	
	for(int i=1; i<=v.size(); i++)			
	{
		cout << setw(3) << v[i];
		count++;
		
		if(i%10==0)
		{
			cout << endl;
		}
	}
	
	sort(v);						
	for(int i=0; i<v.size(); i++)
	{
		cout << v[i];
	}
	
	cout << "Mean = "	<< mean(v)	<< endl;	//mean of value
	cout << "Median = "	<< med(v)	<< endl;	//median of value
	cout << "Mode = "	<< mode(v)	<< endl;	//mode of value
	
	return 0;
}

void sort(vector<int> &v)		
{
	int hold,swap,a;
	do
	{
		swap = 0;
		for(int j=0; j<a[j]-1; j++)
		{
			if(a[j]>[j+1])
			{
				hold	= a[j];
				a[j]	= a[j+1];
				a[j+1]	= hold;
				swap=1;
			}
		}
	}while(swap==1);
}

float mean(int vector<int> &v)		// mean of value
{
	float sum=0;
	for(int i=0; i<a; i++)
	{
		sum=sum+v[i];
	}
	return sum/99;
}

int med(int vector<int> &v)			// median of value
{
	int hold,swap;
	
	do
	{
		swap = 0;
		for(int j=0; j<a[i]-1; j++)
		{
			if(v[j]>[j+1])
			{
				hold	= v[j];
				v[j]	= v[j+1];
				v[j+1]	= hold;
				swap=1;
			}
		}
	}while(swap==1);
	return v[45];
}

int mode(int vector<int> &v)		//mode of value
{
	int freq[10]=[];
	int max;
	int mode=0;
	
	for(int i=0; i<a; i++)
	{
		freq[v[i]]++;
	}
	
	max = freq[1];
	for(int i=0; i<10; i++)
	{
		if(freq[i]>max)
		{
			max = freq[i];
			mode = i;
		}
	}
	return mode;
}
